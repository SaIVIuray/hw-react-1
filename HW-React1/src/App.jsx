import { useState } from 'react';
import ModalText from './components/ModalText';
import ModalImage from './components/ModalImage';
import Button from './components/Button';

import './App.scss';

const App = () => {
  const [showTextModal, setShowTextModal] = useState(false);
  const [showImageModal, setShowImageModal] = useState(false);

  const openTextModal = () => setShowTextModal(true);
  const closeTextModal = () => setShowTextModal(false);

  const openImageModal = () => setShowImageModal(true);
  const closeImageModal = () => setShowImageModal(false);

  return (
    <div className="App">
      <Button onClick={openTextModal}>Modal Text</Button>
      <Button onClick={openImageModal}>Modal Image</Button>
      

      {showTextModal && (
        <ModalText onClose={closeTextModal}>
           <div className='img-close'><img className='img-close-item' onClick={closeTextModal} src="https://kartinki.pics/uploads/posts/2022-12/1670461088_43-kartinkin-net-p-kartinka-krestik-pinterest-51.png" alt="" /></div>
          <h2>Add Product “NAME”</h2>
          <p>Description for you product</p>
          <div className='ButtonApp'>
          <Button onClick={closeTextModal}>Add to favorite</Button>
          </div>
        </ModalText>
      )}

      {showImageModal && (
        <ModalImage onClose={closeImageModal}>
          <div className='img-close'><img className='img-close-item' onClick={closeImageModal} src="https://kartinki.pics/uploads/posts/2022-12/1670461088_43-kartinkin-net-p-kartinka-krestik-pinterest-51.png" alt="" /></div>
          <img className='img' src="https://amiel.club/uploads/posts/2022-03/1647582677_53-amiel-club-p-kartinki-dlya-srisovki-chernoi-ruchkoi-leg-54.jpg" alt="Kitten" />
          <h2>Product Delete!</h2>
          <p>By clicking the “Yes, delete” button, PRODUCT NAME will be deleted.</p>
            <div className='ButtonApp'>
          <Button onClick={closeImageModal}>No, cancel</Button>
          <Button onClick={closeImageModal}>Yes, delete</Button>
          </div>
        </ModalImage>
      )}
    </div>
  );
};

export default App;
