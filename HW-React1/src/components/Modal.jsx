import PropTypes from 'prop-types';

const Modal = ({ children, onClose }) => (
  <div className="modal-background" onClick={onClose}>
    <div className="modal" onClick={(e) => e.stopPropagation()}>
      {children}
    </div>
  </div>
);
  
Modal.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func,
};

export default Modal;
