
import Modal from './Modal';


const ModalText = ({ children }) => <Modal><div className="modal-text">{children}</div></Modal>;

export default ModalText;
