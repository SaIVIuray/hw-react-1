import Modal from './Modal';

const ModalImage = ({ children }) => <Modal><div className="modal-image">{children}</div></Modal>;

export default ModalImage;
