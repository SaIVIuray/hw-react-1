import PropTypes from 'prop-types';
import Button from './Button';

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick }) => (
  <div className="modal-footer">
    {firstText && <Button onClick={firstClick}>{firstText}</Button>}
    {secondaryText && <Button onClick={secondaryClick}>{secondaryText}</Button>}
  </div>
);

ModalFooter.propTypes = {
  firstText: PropTypes.string,
  secondaryText: PropTypes.string,
  firstClick: PropTypes.func,
  secondaryClick: PropTypes.func,
};

export default ModalFooter;
