import PropTypes from 'prop-types';

const ModalClose = ({ onClick }) => <span className="modal-close" onClick={onClick}>&times;</span>;

ModalClose.propTypes = {
  onClick: PropTypes.func,
};

export default ModalClose;
