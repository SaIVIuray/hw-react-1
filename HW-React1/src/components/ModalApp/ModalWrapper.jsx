import PropTypes from 'prop-types';

const ModalWrapper = ({ children }) => (
  <div className="modal-wrapper">
    {children}
  </div>
);

ModalWrapper.propTypes = {
  children: PropTypes.node,
};

export default ModalWrapper;
